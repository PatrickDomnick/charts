{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "generic.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "generic.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "generic.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "generic.labels" -}}
helm.sh/chart: {{ include "generic.chart" . }}
{{ include "generic.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "generic.selectorLabels" -}}
app.kubernetes.io/name: {{ include "generic.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{/*
Create the secret.yml file
*/}}
{{- define "config.yml" -}}
{{- print "" -}}
{{- print "jobs:" | nindent 4 -}}
{{- printf "- name: \"%s\"" .Values.database.database | nindent 4 -}}
{{- printf "interval: %s" .Values.interval | nindent 6 -}}
{{- print "connections:" | nindent 6 -}}
{{- if eq .Values.database.protocol "mysql" -}}
{{- printf "- %s://%s:%s@(%s:%s)/%s" .Values.database.protocol .Values.database.user .Values.database.password .Values.database.host .Values.database.port .Values.database.database | nindent 6 -}}
{{- end -}}
{{- if or (eq .Values.database.protocol "postgres") (eq .Values.database.protocol "sqlserver") -}}
{{- printf "- %s://%s:%s@%s:%s/%s" .Values.database.protocol .Values.database.user .Values.database.password .Values.database.host .Values.database.port .Values.database.database | nindent 6 -}}
{{- end -}}
{{- print "queries:" | nindent 6 -}}
{{ .Values.database.queries | toYaml | nindent 8}}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "generic.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "generic.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Create image pull secrets from supplied credentials
*/}}
{{- define "imagePullSecret" }}
{{- printf "{\"auths\": {\"%s\": {\"auth\": \"%s\"}}}" .Values.artifactory.registry (printf "%s:%s" .Values.artifactory.username .Values.artifactory.password | b64enc) | b64enc }}
{{- end }}
